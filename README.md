# Manual of the game Babylon 5: New Age

This project contains data files from http://b5manual.icsp.sk/ for the [dokuwiki](https://dokuwiki.org/).

## Instalation

[Download dokuwiki](https://download.dokuwiki.org/), extract archive, run webserver and visit `/install.php`.

### Clone this repo to your dokuwiki and you're done.

### Or copy files from original manual by following.

#### Mirror original manual

```
wget -m http://b5manual.icsp.sk/
```

#### Prepare `data/pages/`

From root folder of mirrored manual keep only `*do=export_raw*` files, cut filenames and append `.txt` extension to their names:

```
for I in *do=export_raw*; do mv $I `echo $I | cut -b 27-`.txt ; done
```

Copy remaining files to `<dokuwiki>/data/pages/`

#### Prepare `data/media/`

From `/lib/exe/` folder of the manual keep only `*.php?media=*` files and cut filenames:

```
for I in *.php?media=*; do mv $I "`echo $I | cut -b 17-`" ; done
```

Copy remaining files to `<dokuwiki>/data/media/`.
