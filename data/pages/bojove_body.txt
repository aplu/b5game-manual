====== Bojové body ======

**Bojové body** sú body, ktoré získavaš v útokoch za zničenie alebo aj poškodenie súperovej flotily. Viac o ich získavaní a možnostiach sa dočítaš v tejto časti [[bojove_body_a_bonus_v_utokoch]].

Táto časť zobrazuje poradie TOP 10 získaných bojových bodov ako aj štatitiku celkových získaných bojových bodov za štát.

{{:manual_top10_bojove_body.jpg|}}

{{:manual_bojove_body_za_stat.jpg|}}

