====== Rozvoj hráčov ======

Štatistika hráčov poskytuje prehľad ekonomického a vojenského rozvoja všetkých hráčov. Poradie hráčov podľa bodov za ekonomický rozvoj je dôležitý aj pre [[Prevod|prevody surovín]].

Tvoria ju tieto údaje:
  * poradie hráča,
  * meno hráča,
  * percento z maxima.

POZOR!!! Tieto štatistiky nie sú viditeľné počas prvých 21 dní veku.

{{:manual_statistika_hracov_aktualna.jpg|}}

