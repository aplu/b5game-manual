====== Anketa ======

**V tomto prípade ide o administrátorskú anketu.** Admini (TM) prostrednítvom nej zisťujú názory hráčov na viac alebo menej dôležité veci hry alebo v súvislosti s ňou. Hráči si môžu vybrať z nadefinovaných odpovedí.
Hlasovanie môže byť verejné alebo tajné.

{{:manual_anketa.jpg|}}
