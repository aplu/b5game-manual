====== Výstavba stanice ======

Základným prvkom ekonomiky je stanica a výstavba budov, ktoré ju tvoria. Stanicu ako aj jej budovy nemožno zničiť (do budúcna sa uvažuje s inou možnosťou). **Stanica každého hráča** sa nachádza v jeho domovskom sektore (HW hráča) a možno ju vidieť v náhľade do daného sektora (nevidíš stanice iných hráčov okrem tých hráčov, ktorí majú HW v tom istom sektore ako ty, alebo sú občanmi tvojho alebo aliančného štátu). Stanicu môžes za splnenia podmienok [[Presun stanice|presunúť]] do iného sektora.

(ilustračný obrázok s cenami pre kreditové rasy) 

{{:manual_vystavba.jpg|}}

===== Budovy =====
  * **Taviaca pec** - o přepočtu přetavuje rudu Titánia na Titánium, zabírá 10 místa. Jedna taviaca pec spracuje 1000 ton rudy Titánia. 
  * **Rafinéria** - o přepočtu přetavuje rudu Quantia 40 na Quantium 40, zabírá 60 místa. Jedna rafinéria spracuje 1000 ton rudy q40.
  * **Továreň** - zrychluje stavbu všech lodí (bojových i nebojových) o jedno 1% z plného času, zabírá 150 místa. Maximální počet je 48.
  * **Hangár** - zde parkují letky stihačů, přiřazených k těžbě, nezabírá místo.
  * **Vesmírný dok** - nutný pro stavbu těžebních plošin, křižníků a destroyerů. Vyžaduje min. 5lvl [[Upgrade|Upgradu doků]]. Nezabírá místo.

===== Doba výstavby budov =====
Každá budova se staví jinou dobu a různé počty se staví různě dlouho, viz tabulka:
^           ^ 6 hodin    ^  8 hodin  ^  12 hodin  ^
| Rafinerie |  1 - 5     |  6 - 15    |  16+   |
| Pece      |  1 - 50    |  51 - 150  |  151+  |

^           ^ 10 hodin  ^  12 hodin  ^  16 hodin  ^
| Továrny   |  1 - 3    |  4 - 9    |  10+     |
| Hangár    |  9 hodin    |  lze stavět jen jeden naráz  ||
| Ves. dok  |  12 hodin    |  lze mít jen jeden  ||

===== Suroviny a kredity =====

**Rozlišujeme pět základních surovin:** 

^ Surovina        ^    Zkratka        ^ Popis         ^
| ruda Titánia    |    rTi, rT        |z ní se přetavním v pecích dostává čisté Titánium|
| ruda Quantia 40 |    rQ40, rQ       |z ní se přetavením v rafineriích dostává čisté Quantium 40|
| Titánium        |    Ti, Tit, titko |hlavní stavební prvek, je potřebný naprosto pro všechno|
| Quantium 40     |    Q40, qčko      |ne tak důležité jako Titánium, ale i tak velice potřebné, získává se mnohem hůře|
| Kredity         |    Cr, Kr         |vpodstatě peníze, kupujete za ně všechno možné, Kolónia Drakhov Kredity nemá|


===== Zpracovanie rudy v Taviacich peciach a Rafinériách =====

Zpracování surovin závisí na počtu pecí/rafinerií, na levelu upgradu výroby a velikosti zásob rudy. Každá pec/rafinerie je schopna zpracovat 1000 tun rudy a vynese podle levelu upgradu výroby.

		Př.1 : mám 100 pecí, 200 000 rTi, upgrade lvl 6 upgradu výroby (555tit) => 100pecí * 555 => 55‘500 tun titánia.
		Př.2 : mám 100 pecí, 40 000 rTi, upgrade lvl 6 => 40 pecí (ostatní nemají rudu na přetavení) * 555 = 22‘220 tun Tit
