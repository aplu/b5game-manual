====== Prevod ======

Každý hráč môže previesť časť [[Výstavba#Suroviny a kredity|surovín a kreditov]] inému hráčovi. V hre nie je možné dotovanie hráčov resp. občanov cez národný sklad. Preto je tu možnosť, aby si hráči mohli posielať suroviny medzi sebou a to nie len medzi občanmi jedného štátu ale medzi ktorýmikoľvek hráčmi.

**Prevod funguje jednoducho:**
  * vyberieš príjemcu, množstvo surovín a kreditov a odošleš (prvý obrázok),
  * vyberieš príjemcu, naprázdno dáš odoslať- zobrazí sa ti limit na 3 dni a zvyšný limit surovín, ktoré môžeš poslať konkrétnemu hráčovi (druhý obrázok).

{{:manual_prevod_bez_hraca.jpg|}}

{{:manual_prevod_pre_hraca.jpg|}}

Pod tabuľkami s možnosťou prevodu je tabuľka prevodov za posledné 3 dni.

{{:manual_tabulka_prevodov.jpg|}}

===== Obmedzenia prevodu =====

Ako vidíš, na obrázkoch hore je tabuľka s údajmi o:
  * tvojich surovinách,
  * celkovom limite surovín, ktoré môžes posať za tri dni,
  * tvojom zvyšnom limite,
  * maximálnom prevodu, ktorý môžes v daný moment urobiť,
  * limite na 3 dni a zostávajúcom limite pre konkrétneho hráča (ak klikneš naprádzno odoslať pre konkrétne vybraného hráča).

**Prevod surovín a kreditov má teda svoje obmedzenia. Platí pravidlo, že čím menej je hráč rozvinutý oproti odosielateľovi, tým viac sa mu dá poslať.

!!! V prípade, že chce poslať suroviny a kredity menej rozvinutý hráč viacrozvinutému, nebude to možné - hodnoty limitu budú nulové. Naopak, ak chce poslať viacrozvinutý hráč menej rozvinutému, bude to možné.**
Toto obmedzenie ako aj obmedzenia dané limitom sledujú určitú ochranu hry v tom, aby nebolo možné zneužívať prevody surovín napr. dotovaním len jedného alebo pár hráčov.


**Ako vedieť, ktorý hráč je ako rozvinutý?** V záložke Štatistika je pod položkou štatistika [[Štatistika(hráči)|tabuľka ekonomického a vojenského rozvoja]] všetkých hráčov. Podľa stupňa ekonomického rozvoja vidíš, či sa hráč nachádza pod tebou a teda mu môžes niešo poslať, alebo nad tebou a hodnoty budú nulové.



