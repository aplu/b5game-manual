====== Stanice ======

Niektorí členovia vlády (s príslušnými právomocami) môžu zadať v konkrétnych sektoroch, ktoré patria štátu, **stavbu alebo búranie staníc a skokových brán.** 

**Ťažobná a obchodná stanica** - zvyšujú výnosnoť rúd resp. kreditov o 30% viď [[Ťažba#Množství surovin|množstvo vyťažených surovín]].

**Obranná stanica** - pridáva 20% útoku celej flotile vlastníka sektora a jeho spojencov. **POZOR !!** Zničenie obrannej stanice nastane iba ak pri útoku na sektor prehrá obranca (mení sa vlastníctvo sektora). 

**Skoková brána** - umožňuje ťažbu v mimodomovských sektoroch a urýchľuje [[Presun|presun bojových plavidiel]] (skoky).

Každá stanica a skoková brána má svoje náklady, ktoré treba za stavbu zaplatiť. Dĺžka výstavby staníc je 36 hodín a brán 24 hodín.

**POZOR !!! Ceny staníc na obrázku sú len pre ilustráciu. V hre môže byť "zapnutá" "pohyblivá ekonomika" a potom sa ceny menia** (neplatí pre prvých 10 dní od začiatku veku). Viac o pohyblivej ekonomike resp. pohyblivých cenách sa dočítaš v časti [[makroekonomika]].

{{:manual_stanice.jpg|}}